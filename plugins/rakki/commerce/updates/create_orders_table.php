<?php namespace Rakki\Commerce\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('rakki_commerce_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('item_id');
            $table->string('order_no');
            $table->string('product_category');
            $table->string('product_name');
            $table->string('product_item');
            $table->string('order_name')->nullable();
            $table->string('order_phone')->nullable();
            $table->string('order_email')->nullable();
            $table->string('mikrotik_name')->nullable();
            $table->string('mikrotik_password')->nullable();
            $table->integer('price');
            $table->enum('status', ['hold', 'waiting', 'success', 'cancel', 'expired']);
            $table->string('payment_id')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('payment_key')->nullable();
            $table->string('payment_number')->nullable();
            $table->timestamps();
            $table->datetime('expired_at');
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('rakki_commerce_orders');
    }
}
