<?php namespace Rakki\Commerce;

use App;
use Backend;
use System\Classes\PluginBase;
use Illuminate\Foundation\AliasLoader;
use Event;

/**
 * Commerce Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Commerce',
            'description' => 'No description provided yet...',
            'author'      => 'Rakki',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // // Register ServiceProviders
        // App::register('\jjsquady\MikrotikApi\MikrotikServiceProvider');
        // // Register aliases
        // $alias = AliasLoader::getInstance();
        // $alias->alias('Mikrokit', 'jjsquady\MikrotikApi\Facades\MikrotikFacade');
        // // Register Twig extensions
        // Event::listen('cms.page.beforeDisplay', function($controller, $url, $page) {
        //     $twig = $controller->getTwig();
        //     $twig->addExtension(new \Barryvdh\Debugbar\Twig\Extension\Debug($this->app));
        //     $twig->addExtension(new \Barryvdh\Debugbar\Twig\Extension\Stopwatch($this->app));
        // });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Rakki\Commerce\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Register Mail Templates
     */
    public function registerMailTemplates()
    {
        return [
            'rakki.commerce::mail.commerce-order'   => 'Order information.',
            'rakki.commerce::mail.commerce-summary' => 'Order summary information.',
	    'rakki.commerce::mail.test' => 'test email'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'rakki.commerce.some_permission' => [
                'tab' => 'Commerce',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'commerce' => [
                'label'       => 'Commerce',
                'url'         => Backend::url('rakki/commerce/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['rakki.commerce.*'],
                'order'       => 500,
            ],
        ];
    }
}
