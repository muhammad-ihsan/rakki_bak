<?php namespace Rakki\Commerce\Models;

use Carbon\Carbon;
use Model;

use Rakki\Core\Classes\Generator;

/**
 * Order Model
 */
class Order extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rakki_commerce_orders';

    /**
     * @var string The database table used by the model.
     */
    protected $dates = ['expired_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'logs' => [
            'Rakki\Commerce\Models\OrderLog',
            'id'       => 'order_id',
            'otherKey' => 'id'
        ],
    ];
    public $belongsTo     = [
        'item' => [
            'Rakki\Product\Models\ProductItem',
            'id'       => 'item_id',
            'otherKey' => 'id'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator = new Generator();

        $this->order_no   = $this->item->product->code.date('Ymdhi');
        $this->status     = 'hold';
        $this->expired_at = Carbon::parse($this->created_at)->addMinutes(15);
        $this->parameter  = $generator->make();
    }

    // public function afterCreate()
    // {
    //     $this->item->is_published = 0;
    //     $this->item->save();
    // }

    public function afterUpdate()
    {
        // if($this->status == 'success') {
        //     $this->item->is_published = 0;
        //     $this->item->save();
        // }

        // if($this->status == 'cancel' || $this->status == 'expired') {
        //     $this->item->is_published = 1;
        //     $this->item->save();
        // }
    }
}
