<?php namespace Rakki\Commerce\Models;

use Model;

/**
 * OrderDetail Model
 */
class OrderDetail extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rakki_commerce_order_details';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
