<?php namespace Rakki\Commerce\Models;

use Model;

/**
 * OrderLog Model
 */
class OrderLog extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rakki_commerce_order_logs';

    protected $jsonable = ['body'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
