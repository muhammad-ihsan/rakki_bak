<?php namespace Rakki\Core\Classes;

class Generator
{
    public function make()
    {
        return md5(rand(0, 9999).date('Y-m-d H:i:s'));
    }
}
