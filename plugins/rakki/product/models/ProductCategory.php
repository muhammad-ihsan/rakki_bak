<?php namespace Rakki\Product\Models;

use Model;
use Rakki\Core\Classes\Generator;

/**
 * ProductCategory Model
 */
class ProductCategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rakki_product_product_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'products' => [
            'Rakki\Product\Models\Product',
            'key'      => 'category_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator();
        $this->parameter = $generator->make();
    }

    public function afterDelete()
    {
        $this->products()->delete();
    }
}
