<?php namespace Rakki\Product\Models;

use Model;
use Rakki\Core\Classes\Generator;

/**
 * Product Model
 */
class Product extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rakki_product_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'items' => [
            'Rakki\Product\Models\ProductItem',
            'key'      => 'product_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [
        'category' => [
            'Rakki\Product\Models\ProductCategory',
            'key'      => 'category_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator;
        $this->parameter = $generator->make();
    }

    public function afterDelete()
    {
        $this->items()->delete();
    }
}
