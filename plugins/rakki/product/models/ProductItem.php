<?php namespace Rakki\Product\Models;

use Model;
use Rakki\Core\Classes\Generator;

/**
 * ProductItem Model
 */
class ProductItem extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'rakki_product_product_items';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'product' => [
            'Rakki\Product\Models\Product',
            'id'       => 'product_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator       = new Generator;
        $this->parameter = $generator->make();
    }
}
