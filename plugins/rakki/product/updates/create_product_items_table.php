<?php namespace Rakki\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductItemsTable extends Migration
{
    public function up()
    {
        Schema::create('rakki_product_product_items', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id');
            $table->string('name');
            $table->string('name_mikrotik')->nullable();
            $table->integer('price');
            $table->timestamps();
            $table->boolean('is_published')->nullable()->default(1);
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('rakki_product_product_items');
    }
}
