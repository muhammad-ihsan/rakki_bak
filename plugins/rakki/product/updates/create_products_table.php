<?php namespace Rakki\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('rakki_product_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id');
            $table->string('name');
            $table->string('code');
            $table->text('description');
            $table->timestamps();
            $table->boolean('is_published')->nullable()->default(1);
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('rakki_product_products');
    }
}
