<?php namespace Rakki\Product\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('rakki_product_product_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
            $table->boolean('is_mikrotik')->nullable()->default(0);
            $table->boolean('is_published')->nullable()->default(1);
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('rakki_product_product_categories');
    }
}
