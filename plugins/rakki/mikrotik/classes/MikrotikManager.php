<?php namespace Rakki\Mikrotik\Classes;

use Rakki\Mikrotik\Classes\Mikrotik;
use Rakki\Core\Classes\Generator;

class MikrotikManager
{
    public function createUser($order)
    {
        $mikrokit   = new Mikrotik;
        $connecting = $mikrokit->connect(env('MIKROTIK_DNS', 'rakki.ddns.net'), env('MIKROTIK_USER', 'admin'), env('MIKROTIK_PASSWORD', 'admin'));

        if($connecting) {
            $mikrokit->comm("/tool/user-manager/user/add",array(
                "customer"          => env('MIKROTIK_CUSTOMER', 'admin'),
                "username"          => $order->mikrotik_name,
                "password"          => $order->mikrotik_password,
             ));

    	    $mikrokit->comm("/tool/user-manager/user/create-and-activate-profile", array(
        		"numbers"	=> $order->mikrotik_name,
        		"customer"	=> env('MIKROTIK_CUSTOMER', 'admin'),
        		"profile"	=> $order->item->name_mikrotik,
    	    ));

	    $mikrokit->comm("/tool/sms/send",array(
	             "port" => "usb1",
	             "channel" => "0",
	             "phone-number" => $order->order_phone,
	             "message" => "Rakki TechnoCom Pembelian Voucher Berhasil SSID : Rakki TechnoCom Username : ".$order->mikrotik_name." Password : ".$order->mikrotik_password,
            ));


            return true;
        }

        return false;
    }
}
