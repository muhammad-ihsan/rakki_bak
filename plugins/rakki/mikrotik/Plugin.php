<?php namespace Rakki\Mikrotik;

use Backend;
use System\Classes\PluginBase;

/**
 * Mikrotik Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Mikrotik',
            'description' => 'No description provided yet...',
            'author'      => 'Rakki',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Rakki\Mikrotik\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'rakki.mikrotik.some_permission' => [
                'tab' => 'Mikrotik',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'mikrotik' => [
                'label'       => 'Mikrotik',
                'url'         => Backend::url('rakki/mikrotik/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['rakki.mikrotik.*'],
                'order'       => 500,
            ],
        ];
    }
}
