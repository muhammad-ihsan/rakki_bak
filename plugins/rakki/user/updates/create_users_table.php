<?php namespace Rakki\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('rakki_user_users', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->timestamps();
            $table->boolean('is_active')->nullable()->default(1);
            $table->boolean('is_admin')->nullable()->default(0);
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('rakki_user_users');
    }
}
