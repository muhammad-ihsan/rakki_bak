<?php namespace Web\Auth\Components;

use Hash;
use Redirect;
use Flash;
use Session;
use Validator;

use Rakki\User\Models\User;

use Cms\Classes\ComponentBase;

class Auth extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Auth Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = $this->getUser();
        if($user) {
            return Redirect::to('/admin/dashboard');
        }
    }

    public function onLogin()
    {
        $rules = [
            'email'    => 'required|email',
            'password' => 'required',
        ];
        $messages       = [];
        $attributeNames = [];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $user = User::whereEmail(post('email'))->first();
        if($user) {
            if(Hash::check(post('password'), $user->password)) {
                if($user->is_active) {
                    $this->putSession($user);
                    Flash::success('Selamat Datang '.$user->email);
                    return Redirect::to('/admin/dashboard');
                }

                Flash::info('Akun kamu belum diaktifkan');
                return;
            }

            Flash::error('Alamat email atau password tidak sesuai');
            return;
        }

        Flash::error('Alamat email tidak ditemukan');
        return;
    }

    public function getUser()
    {
        return Session::get('userLogin');
    }

    public function putSession($user)
    {
        return Session::put('userLogin', $user);
    }
}
