<?php namespace Web\User\Components;

use Carbon\Carbon;
use Flash;
use Redirect;
use Exception;

use Veritrans_Config;
use Veritrans_Transaction;

use Rakki\Commerce\Models\Order;

use Cms\Classes\ComponentBase;

class Finish extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Finish Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $order       = $this->getCurrent();
        if(!$order) {
            Flash::error('Pemesanan tidak ditemukan');
            return Redirect::to('/');
        }

        $transaction = $this->getMidtrans();
        if(!$transaction && $order) {
            return Redirect::to('/payment/'.$order->parameter);
        }

        $this->page['order']       = $order;
        $this->page['transaction'] = $transaction;
    }

    public function getMidtrans()
    {
	Veritrans_Config::$serverKey    = env('MIDTRANS_SERVER_KEY');
        Veritrans_Config::$isProduction = true; //env('MIDTRANS_IS_PRODUCTION') ? true : false;
        Veritrans_Config::$isSanitized  = true; //env('MIDTRANS_IS_SANITIZED') ? true : false;
        Veritrans_Config::$is3ds        = true; //env('MIDTRANS_IS_IS3DS') ? true : false;


        $order       = $this->getCurrent();
        if($order) {
            try {
                $transaction = Veritrans_Transaction::status($order->order_no);
                return $transaction;
            } catch (Exception $e) {
                return false;
            }
        }
    }

    public function getCurrent()
    {
        return Order::whereParameter($this->property('parameter'))->first();
    }
}
