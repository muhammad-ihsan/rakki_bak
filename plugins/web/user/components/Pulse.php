<?php namespace Web\User\Components;

use Carbon\Carbon;
use Mail;
use Flash;
use Redirect;

use Veritrans_Config;
use Veritrans_Notification;

use Rakki\Commerce\Models\Order;
use Rakki\Commerce\Models\OrderLog;

use Rakki\Mikrotik\Classes\MikrotikManager;

use Cms\Classes\ComponentBase;

class Pulse extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Pulse Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        Veritrans_Config::$serverKey    = env('MIDTRANS_SERVER_KEY');
        Veritrans_Config::$isProduction = true; //env('MIDTRANS_IS_PRODUCTION') ? true : false;
        Veritrans_Config::$isSanitized  = true; //env('MIDTRANS_IS_SANITIZED') ? true : false;
        Veritrans_Config::$is3ds        = true; //env('MIDTRANS_IS_IS3DS') ? true : false;

        $notif                          = new Veritrans_Notification;
        $transaction                    = $notif->transaction_status;
        $type                           = $notif->payment_type;
        $fraud                          = $notif->fraud_status;

        $order                          = $this->getCurrent($notif->order_id);
        $order->payment_type            = $notif->payment_type;
        $order->created_at              = $notif->transaction_time;
        $order->expired_at              = Carbon::parse($order->created_at)->addDays(env('MIDTRANS_EXPIRATION'));

        $usermanName                    = substr(md5(date('Y-m-d H:i:s')), 0, 6);
        $usermanPass                    = substr(md5($usermanName), 0, 6);

        if ($transaction == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
                if($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    // echo "Transaction order_id: " . $order_id ." is challenged by FDS";
                }
                else {
                    if($order->status != 'success') {
                        // TODO set payment status in merchant's database to 'Success'
                        $order->status            = 'success';
                        $order->mikrotik_name     = $usermanName;
                        $order->mikrotik_password = $usermanPass;
                        $order->save();

                        // If its internet product
                        if($order->item->product->category->is_mikrotik) {
                            $mikrokit = new MikrotikManager;
                            $mikrokit->createUser($order);
                        }

                        $this->createOrderLog($order);

                        // Send Email
                        $mailVars = [
                            'order' =>  $order
                        ];
                        Mail::send('rakki.commerce::mail.commerce-summary', $mailVars, function($message) use ($order) {
                            $message->to($order->order_email, $order->order_phone);
                            $message->subject('Pembayaran kamu telah kami terima');
                        });

			Mail::send('rakki.commerce::mail.commerce-internet-voucher', $mailVars, function($message) use ($order) {
                            $message->to($order->order_email, $order->order_phone);
                            $message->subject('Informasi Voucher Internet');
                        });
                    }
                }
            }
        }
        else if ($transaction == 'settlement') {
            if($order->status != 'success') {
                // TODO set payment status in merchant's database to 'Settlement'
                $order->status            = 'success';
                $order->mikrotik_name     = $usermanName;
                $order->mikrotik_password = $usermanPass;
                $order->save();

                // If its internet product
                if($order->item->product->category->is_mikrotik) {
                    $mikrokit = new MikrotikManager;
                    $mikrokit->createUser($order);
                }

                $this->createOrderLog($order);

                // Send Email
                $mailVars = [
                    'order' =>  $order
                ];
                Mail::send('rakki.commerce::mail.commerce-summary', $mailVars, function($message) use ($order) {
                    $message->to($order->order_email, $order->order_phone);
                    $message->subject('Pembayaran kamu telah kami terima');
                });

		Mail::send('rakki.commerce::mail.commerce-internet-voucher', $mailVars, function($message) use ($order) {
                    $message->to($order->order_email, $order->order_phone);
                    $message->subject('Informasi Voucher Internet');
                });
            }
        }
        else if($transaction == 'pending') {
            if($order->status != 'waiting') {
                // TODO set payment status in merchant's database to 'Pending'
                $order->status       = 'waiting';
                $order->save();

                $this->createOrderLog($order);

                // Send Email
                $mailVars = [
                    'order' =>  $order
                ];
                Mail::send('rakki.commerce::mail.commerce-order', $mailVars, function($message) use ($order) {
                    $message->to($order->order_email, $order->order_phone);
                    $message->subject('Informasi pemesanan anda');
                });
            }
        }
        else if ($transaction == 'deny') {
            if($order->status != 'cancel') {
                // TODO set payment status in merchant's database to 'Denied'
                $order->status = 'cancel';
                $order->save();

                $this->createOrderLog($order);

                // Send Email
                $mailVars = [
                    'order' =>  $order
                ];
                Mail::send('rakki.commerce::mail.commerce-summary', $mailVars, function($message) use ($order) {
                    $message->to($order->order_email, $order->order_phone);
                    $message->subject('Pemesanan kamu telah di batalkan');
                });
            }
        }
        else if ($transaction == 'expire') {
            if($order->status != 'expired') {
                // TODO set payment status in merchant's database to 'expire'
                $order->status = 'expired';
                $order->save();
                $this->createOrderLog($order);

                // Send Email
                $mailVars = [
                    'order' =>  $order
                ];
                Mail::send('rakki.commerce::mail.commerce-summary', $mailVars, function($message) use ($order) {
                    $message->to($order->order_email, $order->order_phone);
                    $message->subject('Pemesanan kamu sudah jatuh tempo');
                });
            }
        }
        else if ($transaction == 'cancel') {
            if($order->status != 'cancel') {
                // TODO set payment status in merchant's database to 'Denied'
                $order->status = 'cancel';
                $order->save();
                $this->createOrderLog($order);

                // Send Email
                $mailVars = [
                    'order' =>  $order
                ];
                Mail::send('rakki.commerce::mail.commerce-summary', $mailVars, function($message) use ($order) {
                    $message->to($order->order_email, $order->order_phone);
                    $message->subject('Pemesanan kamu telah di batalkan');
                });
            }
        }
    }

    public function getCurrent($orderNo)
    {
        return Order::whereOrderNo($orderNo)->first();
    }

    public function createOrderLog($order)
    {
        $log            = new OrderLog;
        $log->order_id  = $order->id;
        $log->body      = $order;
        $log->save();
    }
}
