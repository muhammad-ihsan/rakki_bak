<?php namespace Web\User\Components;

use Validator;
use Flash;

use Rakki\Commerce\Models\Order;

use Cms\Classes\ComponentBase;

class Commerce extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Commerce Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onTrack()
    {
        $rules = [
            'phone' => 'required|numeric',
        ];
        $messages       = [];
        $attributeNames = [
            'phone'     => 'telefon',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $order               = Order::orderBy('created_at', 'desc')->whereOrderPhone(post('phone'))->get();
        $this->page['orders'] = $order;
        $this->page['find']   = true;
    }

    public function getAll()
    {
        return Order::orderBy('created_at', 'desc')->paginate(10);
    }
}
