<?php namespace Web\User\Components;

use Flash;
use Redirect;
use Validator;

use Rakki\Commerce\Models\Order;

use Cms\Classes\ComponentBase;

class Checkout extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Checkout Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $order = $this->getCurrent();
        if(!$order) {
            Flash::error('Pemesanan tidak ditemukan');
            return Redirect::to('/');
        }

        if($order->status != 'hold') {
            return Redirect::to('/finish/'.$order->parameter);
        }

        $this->page['order'] = $order;
    }

    public function getCurrent()
    {
        return Order::whereParameter($this->property('parameter'))->first();
    }


    public function onCheckout()
    {
        $rules = [
            'name'  => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',
        ];
        $messages       = [];
        $attributeNames = [
            'name'  => 'nama',
            'phone' => 'telefon',
            'email' => 'email',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $order                = $this->getCurrent();
        $order->order_name  = post('name');
        $order->order_phone = post('phone');
        $order->order_email = post('email');
        $order->save();
        Flash::success('Pemesanan berhasil dibuat');
        return Redirect::to('/payment/'.$order->parameter);
    }
}
