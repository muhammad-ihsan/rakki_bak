<?php namespace Web\User\Components;

use Flash;
use Session;
use Redirect;

use Rakki\Product\Models\Product;
use Rakki\Product\Models\ProductCategory;
use Rakki\Product\Models\ProductItem;

use Cms\Classes\ComponentBase;

class Catalog extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Catalog Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getCategories()
    {
        return ProductCategory::orderBy('name', 'asc')->whereIsPublished(1)->get();
    }

    public function onGetProduct()
    {
        $products = Product::whereCategoryId(post('category_id'))->whereIsPublished(1)->orderBy('name', 'asc')->get();
        $this->page['products'] = $products;
    }

    public function onGetProductItem()
    {
        $products                    = ProductItem::whereProductId(post('product_id'))->whereIsPublished(1)->get();
        $this->page['products']      = $products;
    }

    public function onGetProductItemPrice()
    {
        $product                    = ProductItem::whereId(post('item_id'))->whereIsPublished(1)->first();
        $this->page['product']      = $product;
    }
}
