<?php namespace Web\User\Components;

use Cms\Classes\ComponentBase;

class CommerceDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'CommerceDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
