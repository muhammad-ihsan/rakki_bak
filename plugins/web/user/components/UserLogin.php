<?php namespace Web\User\Components;

use Flash;
use Redirect;
use Session;

use Rakki\User\Models\User;

use Cms\Classes\ComponentBase;

class UserLogin extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserLogin Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = $this->getUser();
        if($user) {
            $this->page['userLogin'] = User::whereId($user->id)->First();
            return;
        }
    }

    public function getUser()
    {
        return Session::get('userLogin');
    }

    public function onLogout()
    {
        Session::forget('userLogin');
        Flash::success('Berhasil Keluar');
        return Redirect::to('/');
    }
}
