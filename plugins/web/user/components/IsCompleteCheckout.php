<?php namespace Web\User\Components;

use Flash;
use Redirect;

use Rakki\Commerce\Models\Order;

use Cms\Classes\ComponentBase;

class IsCompleteCheckout extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'IsCompleteCheckout Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $order = $this->getCurrent();
        if(
            !$order->order_name &&
            !$order->order_phone &&
            !$order->order_email
        ) {
            return Redirect::to('/checkout/'.$order->parameter);
        }
    }

    public function getCurrent()
    {
        return Order::whereParameter($this->property('parameter'))->first();
    }
}
