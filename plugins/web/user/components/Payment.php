<?php namespace Web\User\Components;

use Flash;
use Redirect;

use Veritrans_Config;
use Veritrans_Snap;

use Rakki\Commerce\Models\Order;

use Cms\Classes\ComponentBase;

class Payment extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Payment Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $order = $this->getCurrent();

        if(!$order) {
            Flash::error('Pemesanan tidak ditemukan');
            return Redirect::to('/');
        }

        if($order->status != 'hold') {
            return Redirect::to('/finish/'.$order->parameter);
        }

        // Set your Merchant Server Key
        Veritrans_Config::$serverKey    = env('MIDTRANS_SERVER_KEY');
        Veritrans_Config::$isProduction = true; //env('MIDTRANS_IS_PRODUCTION') ? true : false;
        Veritrans_Config::$isSanitized  = true; //env('MIDTRANS_IS_SANITIZED') ? true : false;
        Veritrans_Config::$is3ds        = true; //env('MIDTRANS_IS_IS3DS') ? true : false;

        $items = array(
            array(
                'id'       => $order->item->id,
                'price'    => $order->price,
                'quantity' => 1,
                'name'     => $order->item->product->name.' '.$order->item->name
            ),
        );
        $params = [
            'transaction_details'   => [
                'order_id'     => $order->order_no,
                'gross_amount' => $order->price,
            ],
            'item_details'          => $items,
            'customer_details'      => [
                'first_name'    => $order->order_email,
                'email'         => $order->order_email,
                'phone'         => $order->order_phone,
            ]
        ];
        $snapToken = Veritrans_Snap::getSnapToken($params);

        $this->page['order']       = $order;
        $this->page['snapToken']   = $snapToken;
        $this->page['redirectUrl'] = env('APP_URL').'finish/'.$order->parameter;
    }

    public function getCurrent()
    {
        return Order::whereParameter($this->property('parameter'))->first();
    }
}
