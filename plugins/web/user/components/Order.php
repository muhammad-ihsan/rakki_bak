<?php namespace Web\User\Components;

use Mail;
use Flash;
use Redirect;
use Validator;

use Rakki\Product\Models\Product;
use Rakki\Product\Models\ProductItem;

use Rakki\Commerce\Models\Order as OrderModel;
use Rakki\Commerce\Models\OrderLog;

use Cms\Classes\ComponentBase;

class Order extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Order Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onOrder()
    {
        $rules = [
            'category_id' => 'required|numeric',
            'product_id'  => 'required|numeric',
            'item_id'     => 'required|numeric',
            'phone'       => 'required|numeric|phone_id',
            'email'       => 'required|email',
        ];
        $messages       = [
            'phone_id' => 'Kolom :attribute tidak sesuai format',
        ];
        $attributeNames = [
            'category_id' => 'produk',
            'product_id'  => 'provider',
            'item_id'     => 'nominal',
            'phone'       => 'telefon',
            'email'       => 'email',
        ];

        Validator::extend('phone_id', function($attr, $value){
            return preg_match('/^(^\08\s?|^0)(\d{3,4}?){2}\d{3,4}$/', $value);
        });

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        // Validator for product item
        $product = $this->getProduct(post('product_id'));
        if(!$product || $product->is_published == 0) {
            Flash::error('Provider tidak tersedia');
            return Redirect::refresh();
        }

        $item = $this->getProductItem(post('item_id'));
        if(!$item || $item->is_published == 0) {
            Flash::error('Item tidak tersedia');
            return Redirect::refresh();
        }

        $order                   = new OrderModel;
        $order->item_id          = $item->id;
        $order->product_category = $item->product->category->name;
        $order->product_name     = $item->product->name;
        $order->product_item     = $item->name;
        $order->price            = $item->price;
        $order->order_phone      = post('phone');
        $order->order_email      = post('email');
        $order->save();

        // Redirect finish page
        Flash::success('Pemesanan berhasil dibuat');
        return Redirect::to('payment/'.$order->parameter);
    }

    public function getProduct($productId)
    {
        $product = Product::whereId($productId)->first();
        return $product;
    }

    public function getProductItem($itemId)
    {
        $item = ProductItem::whereId($itemId)->first();
        return $item;
    }
}
