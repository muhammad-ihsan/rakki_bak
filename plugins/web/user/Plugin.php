<?php namespace Web\User;

use Backend;
use System\Classes\PluginBase;

/**
 * User Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'User',
            'description' => 'No description provided yet...',
            'author'      => 'Web',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            // User Purpose
            'Web\User\Components\UserLogin'  => 'UserLogin',


            // Product Purpose
            'Web\User\Components\Catalog'  => 'Catalog',
            'Web\User\Components\Order'    => 'Order',
            'Web\User\Components\Checkout' => 'Checkout',
            'Web\User\Components\Payment'  => 'Payment',
            'Web\User\Components\Finish'   => 'Finish',
            'Web\User\Components\Invoice'  => 'Invoice',
            'Web\User\Components\Pulse'    => 'Pulse',

            'Web\User\Components\Commerce'    => 'Commerce',
            'Web\User\Components\CommerceDetail'    => 'CommerceDetail',


            'Web\User\Components\IsCompleteCheckout'   => 'IsCompleteCheckout',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'web.user.some_permission' => [
                'tab' => 'User',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'user' => [
                'label'       => 'User',
                'url'         => Backend::url('web/user/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['web.user.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'encode'        => [$this, 'makeJsonEncode'],
                'number_format' => [$this, 'makeNumerFormat'],
                'phone_mask'    => [$this, 'makePhoneMask']
            ],
        ];
    }

    public function makeNumerFormat($text)
    {
        return number_format($text);
    }

    public function makeJsonEncode($text)
    {
        return json_encode($text);
    }

    public function makePhoneMask($text)
    {
        return substr_replace($text, 'xxxxx', 5);
    }
}
