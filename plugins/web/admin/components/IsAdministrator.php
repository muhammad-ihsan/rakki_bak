<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class IsAdministrator extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'IsAdministrator Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
