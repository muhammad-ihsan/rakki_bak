<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Session;

use Cms\Classes\ComponentBase;

class IsLogin extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'IsLogin Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = $this->getUser();
        if(!$user) {
            Flash::error('Terjadi kesalahan');
            return Redirect::back();
        }
    }

    public function getUser()
    {
        return Session::get('userLogin');
    }
}
