<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Rakki\Product\Models\Product;
use Rakki\Product\Models\ProductCategory;

use Cms\Classes\ComponentBase;

class AdminProvider extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminProvider Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return Product::orderBy('name', 'asc')->get();
    }

    public function getCategory()
    {
        return ProductCategory::orderBy('name', 'asc')->whereIsPublished(1)->get();
    }

    public function onSave()
    {
        $rules = [
            'category_id'  => 'required',
            'name'         => 'required',
            'code'         => 'required',
            'is_published' => 'required|boolean',
        ];
        $messages       = [];
        $attributeNames = [
            'category_id'  => 'kategori',
            'name'         => 'nama',
            'code'         => 'kode',
            'is_published' => 'status',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $product               = new Product;
        $product->category_id  = post('category_id');
        $product->name         = post('name');
        $product->code         = post('code');
        $product->description  = post('description');
        $product->is_published = post('is_published');
        $product->save();
        Flash::success('Provider berhasil di tambah');
        return Redirect::refresh();
    }

    public function onDelete()
    {
        Product::whereParameter(post('parameter'))->first()->delete();
        Flash::success('Provider berhasil di hapus');
        return Redirect::refresh();
    }
}
