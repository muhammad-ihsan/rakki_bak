<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;
use Hash;
use Session;

use Rakki\User\Models\User;

use Cms\Classes\ComponentBase;

class AdminUser extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminUser Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return User::orderBy('email')->get();
    }

    public function getUser()
    {
        return Session::get('userLogin');
    }


    public function onRequestNew()
    {
        $this->page['new'] = true;
    }

    public function onRequestUpdatePassword()
    {
        $this->page['update_password'] = true;
    }

    public function onSave()
    {
        $rules = [
            'email'           => 'required|unique:rakki_user_users',
            'password'        => 'required|min:6',
            'retype_password' => 'required|min:6|same:password',
        ];
        $messages       = [];
        $attributeNames = [
            'retype_password' => 'Ulangi Password',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $user           = new User;
        $user->email    = post('email');
        $user->password = Hash::make(post('password'));
        $user->save();
        Flash::success('User berhasil di buat');
        return Redirect::refresh();
    }

    public function onUpdatePassword()
    {
        $rules = [
            'password_old'      => 'required|min:6',
            'password_new'      => 'required|min:6',
            'retype_password'   => 'required|min:6|same:password_new',
        ];
        $messages       = [];
        $attributeNames = [
            'password_old'    => 'Password Lama',
            'password_new'    => 'Password Baru',
            'retype_password' => 'Ulangi Password',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $user = $this->getUser();
        $user = User::whereId($user->id)->first();
        if(Hash::check(post('password_old'), $user->password)) {
            $user->password = Hash::make(post('password_new'));
            $user->save();
            Flash::success('Password berhasil di ubah');
            Session::forget('userLogin');
            return Redirect::to('/login');
        }

        Flash::error('Password lama tidak sesuai');
        return;
    }

    public function onSetActive()
    {
        $user = User::whereParameter(post('parameter'))->first();
        if($user->is_active) {
            $user->is_active = 0;
            $flash           = 'User berhasil di nonaktifkan';
        }
        else {
            $user->is_active = 1;
            $flash           = 'User berhasil di aktifkan';

        }
        $user->save();

        Flash::success($flash);
        return Redirect::refresh();
    }

    public function onDelete()
    {
        $user = User::whereParameter(post('parameter'))->first()->delete();
        Flash::success('User berhasil di hapus');
        return Redirect::refresh();
    }
}
