<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminUserDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminUserDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
