<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Rakki\Product\Models\Product;
use Rakki\Product\Models\ProductItem;

use Cms\Classes\ComponentBase;

class AdminProviderPriceDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminProviderPriceDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $product = $this->getCurrent();
        if(!$product) {
            Flash::error('Nominal tidak ditemukan');
            return Redirect::back();
        }

        $this->page['item'] = $product;
    }

    public function getCurrent()
    {
        return ProductItem::whereParameter($this->property('parameter'))->first();
    }

    public function isInternet($productId)
    {
        $provider = Product::whereId($productId)->first();

        if($provider) {
            if($provider->category->is_mikrotik) {
                return true;
            }
        }
    }

    public function onSave()
    {
        $rules = [
            'category_id'  => 'required|numeric',
            'product_id'   => 'required|numeric',
            'name'         => 'required',
            'price'        => 'required|numeric',
            'is_published' => 'required|boolean',
        ];
        $messages       = [];
        $attributeNames = [
            'category_id'  => 'produk',
            'product_id'   => 'provider',
            'name'         => 'nama',
            'price'        => 'harga',
            'is_published' => 'status',
        ];

        if($this->isInternet(post('product_id'))) {
            $isInternetRules          = ['name_mikrotik' => 'required'];
            $isInternetAttributeNames = ['name_mikrotik' => 'profiler mikrotik'];

            $rules          = array_merge($rules, $isInternetRules);
            $attributeNames = array_merge($attributeNames, $isInternetAttributeNames);
        }

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $product               = $this->getCurrent();
        $product->product_id   = post('product_id');
        $product->name         = post('name');
        $product->price        = post('price');
        $product->name_mikrotik= post('name_mikrotik');
        $product->is_published = post('is_published');
        $product->save();
        Flash::success('Nominal berhasil di tambah');
        return Redirect::refresh();
    }
}
