<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Veritrans_Config;
use Veritrans_Transaction;

use Rakki\Commerce\Models\Order;

use Cms\Classes\ComponentBase;

class AdminCommerce extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminCommerce Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return Order::orderBy('created_at', 'desc')->get();
    }

    public function onCancel()
    {
        $order  = Order::whereParameter(post('parameter'))->first();

        if($order) {
            Veritrans_Config::$serverKey    = env('MIDTRANS_SERVER_KEY');
            Veritrans_Config::$isProduction = env('MIDTRANS_IS_PRODUCTION') ? true : false;
            Veritrans_Config::$isSanitized  = env('MIDTRANS_IS_SANITIZED') ? true : false;
            Veritrans_Config::$is3ds        = env('MIDTRANS_IS_IS3DS') ? true : false;

            $transaction = Veritrans_Transaction::cancel($order->order_no);
            if($transaction && $transaction->transaction_status == 'waiting') {
                Flash::success('Transaksi berhasil di batalkan');
                return Redirect::refresh();
            }

            Flash::error('Terjadi kesalahan');
            return Redirect::refresh();
        }

        Flash::error('Transaksi tidak ditemukan');
        return Redirect::refresh();
    }
}
