<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Rakki\Product\Models\Product;
use Rakki\Product\Models\ProductCategory;
use Rakki\Product\Models\ProductItem;

use Cms\Classes\ComponentBase;

class AdminProviderPrice extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminProviderPrice Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return ProductItem::orderBy('name', 'asc')->get();
    }

    public function getCategories()
    {
        return ProductCategory::orderBy('name', 'asc')->whereIsPublished(1)->get();
    }

    public function getProvider()
    {
        return Product::orderBy('name', 'asc')->whereIsPublished(1)->get();
    }

    public function isInternet($productId)
    {
        $provider = Product::whereId($productId)->first();

        if($provider) {
            if($provider->category->is_mikrotik) {
                return true;
            }
        }

        return false;
    }

    public function onChangeProduct()
    {
        $product                = Product::whereCategoryId(post('category_id'))->whereIsPublished(1)->get();
        $this->page['products'] = $product;

         if(count($product)) {
            if($product[0]->category->is_mikrotik) {
                $this->page['is_internet'] = true;
                return true;
            }
        }
    }

    public function onSave()
    {
        $rules = [
            'category_id'  => 'required|numeric',
            'product_id'   => 'required|numeric',
            'name'         => 'required',
            'price'        => 'required|numeric',
            'is_published' => 'required|boolean',
        ];
        $messages       = [];
        $attributeNames = [
            'category_id'  => 'produk',
            'product_id'   => 'provider',
            'name'         => 'nama',
            'price'        => 'harga',
            'is_published' => 'status',
        ];

        if($this->isInternet(post('product_id'))) {
            $isInternetRules          = ['name_mikrotik' =>'required'];
            $isInternetAttributeNames = ['name_mikrotik' =>'profiler mikrotik'];

            $rules          = array_merge($rules, $isInternetRules);
            $attributeNames = array_merge($attributeNames, $isInternetAttributeNames);
        }

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $product               = new ProductItem;
        $product->product_id   = post('product_id');
        $product->name         = post('name');
        $product->price        = post('price');
        $product->name_mikrotik= post('name_mikrotik');
        $product->is_published = post('is_published');
        $product->save();
        Flash::success('Nominal berhasil di tambah');
        return Redirect::refresh();
    }

    public function onDelete()
    {
        ProductItem::whereParameter(post('parameter'))->first()->delete();
        Flash::success('Nominal berhasil di hapus');
        return Redirect::refresh();
    }
}
