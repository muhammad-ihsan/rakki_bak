<?php namespace Web\Admin\Components;

use Mail;
use Flash;
use Redirect;
use Validator;

use Rakki\Mikrotik\Classes\MikrotikManager;

use Cms\Classes\ComponentBase;

use Rakki\Product\Models\Product;
use Rakki\Product\Models\ProductItem;

use Rakki\Commerce\Models\Order as OrderModel;
use Rakki\Commerce\Models\OrderLog;

class AdminUserMikrotik extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminUserMikrotik Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        # code...
    }

    public function getAll()
    {
        return OrderModel::whereStatus('success')->get();
    }

    public function getProduct($productId)
    {
        $product = Product::whereId($productId)->first();
        return $product;
    }

    public function getProductItem($itemId)
    {
        $item = ProductItem::whereId($itemId)->first();
        return $item;
    }



    public function onSave()
    {
        $rules = [
            'category_id' => 'required|numeric',
            'product_id'  => 'required|numeric',
            'item_id'     => 'required|numeric',
            'phone'       => 'required|numeric|phone_id',
            'email'       => 'required|email',
        ];
        $messages       = [
            'phone_id' => 'Kolom :attribute tidak sesuai format',
        ];
        $attributeNames = [
            'category_id' => 'produk',
            'product_id'  => 'provider',
            'item_id'     => 'nominal',
            'phone'       => 'telepon',
            'email'       => 'email',
        ];

        Validator::extend('phone_id', function($attr, $value){
            return preg_match('/^(^\08\s?|^0)(\d{3,4}?){2}\d{3,4}$/', $value);
        });

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        // Validator for product item
        $product = $this->getProduct(post('product_id'));
        if(!$product || $product->is_published == 0) {
            Flash::error('Provider tidak tersedia');
            return Redirect::refresh();
        }

        $item = $this->getProductItem(post('item_id'));
        if(!$item || $item->is_published == 0) {
            Flash::error('Item tidak tersedia');
            return Redirect::refresh();
        }

        /**
         * Generate Mikrotik Account
         */
        $usermanName = substr(md5(date('Y-m-d H:i:s')), 0, 6);
        $usermanPass = substr(md5($usermanName), 0, 6);

        $order                    = new OrderModel;
        $order->item_id           = $item->id;
        $order->product_category  = $item->product->category->name;
        $order->product_name      = $item->product->name;
        $order->product_item      = $item->name;
        $order->price             = $item->price;
        $order->order_phone       = post('phone');
        $order->order_email       = post('email');
        $order->status            = 'success';
        $order->mikrotik_name     = $usermanName;
        $order->mikrotik_password = $usermanPass;
        $order->is_system         = 0;
        $order->save();

        $k = OrderModel::whereId($order->id)->update([
            'status' => 'success'
        ]);


        // If its internet product
        if($order->item->product->category->is_mikrotik) {
            $mikrokit = new MikrotikManager;
            $mikrokit->createUser($order);
        }

        $this->createOrderLog($order);

        // Send Email
        $mailVars = [
            'order' =>  $order
        ];

        Mail::send('rakki.commerce::mail.commerce-summary', $mailVars, function($message) use ($order) {
            $message->to($order->order_email, $order->order_phone);
            $message->subject('Pembayaran kamu telah kami terima');
        });

        Mail::send('rakki.commerce::mail.commerce-internet-voucher', $mailVars, function($message) use ($order) {
            $message->to($order->order_email, $order->order_phone);
            $message->subject('Informasi Voucher Internet');
        });

        // Redirect finish page
        Flash::success('Akun berhasil disimpan');
        return Redirect::refresh();
    }

    public function createOrderLog($order)
    {
        $log            = new OrderLog;
        $log->order_id  = $order->id;
        $log->body      = $order;
        $log->save();
    }
}
