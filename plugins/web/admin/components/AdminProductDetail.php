<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Rakki\Product\Models\ProductCategory;

use Cms\Classes\ComponentBase;

class AdminProductDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminProductDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $product = $this->getCurrent();
        if(!$product) {
            Flash::error('Produk tidak ditemukan');
            return Redirect::back();
        }

        $this->page['product'] = $product;
    }

    public function getCurrent()
    {
        return ProductCategory::whereParameter($this->property('parameter'))->first();
    }

    public function onSave()
    {
        $rules = [
            'name'         => 'required',
            'is_mikrotik'  => 'required|boolean',
            'is_published' => 'required|boolean',
        ];
        $messages       = [];
        $attributeNames = [
            'name'         => 'nama',
            'is_mikrotik'  => 'mikrotik',
            'is_published' => 'status',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $product               = $this->getCurrent();
        $product->name         = post('name');
        $product->is_published = post('is_published');
        $product->is_mikrotik  = post('is_mikrotik');
        $product->save();
        Flash::success('Produk berhasil di ubah');
        return Redirect::refresh();
    }
}
