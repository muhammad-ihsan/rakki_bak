<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Rakki\Product\Models\Product;
use Rakki\Product\Models\ProductCategory;

use Cms\Classes\ComponentBase;

class AdminProviderDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminProviderDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'title'       => 'parameter',
                'description' => 'Wording to display when no file is uploaded',
            ],
        ];
    }

    public function onRun()
    {
        $product = $this->getCurrent();
        if(!$product) {
            Flash::error('Provider tidak ditemukan');
            return Redirect::back();
        }

        $this->page['product'] = $product;
    }

    public function getCurrent()
    {
        return Product::whereParameter($this->property('parameter'))->first();
    }

    public function onSave()
    {
        $rules = [
            'category_id'  => 'required',
            'name'         => 'required',
            'code'         => 'required',
            'is_published' => 'required|boolean',
        ];
        $messages       = [];
        $attributeNames = [
            'category_id'  => 'kategori',
            'name'         => 'nama',
            'code'         => 'kode',
            'is_published' => 'status',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $product               = $this->getCurrent();
        $product->category_id  = post('category_id');
        $product->name         = post('name');
        $product->code         = post('code');
        $product->description  = post('description');
        $product->is_published = post('is_published');
        $product->save();
        Flash::success('Provider berhasil di ubah');
        return Redirect::refresh();
    }
}
