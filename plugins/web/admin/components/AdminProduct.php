<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Rakki\Product\Models\ProductCategory;

use Cms\Classes\ComponentBase;

class AdminProduct extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminProduct Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return ProductCategory::orderBy('name', 'asc')->get();
    }

    public function onSave()
    {
        $rules = [
            'name'         => 'required',
            'is_mikrotik'  => 'required|boolean',
            'is_published' => 'required|boolean',
        ];
        $messages       = [];
        $attributeNames = [
            'name'         => 'nama',
            'is_mikrotik'  => 'mikrotik',
            'is_published' => 'status',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return false;
        }

        $product               = new ProductCategory;
        $product->name         = post('name');
        $product->is_published = post('is_published');
        $product->is_mikrotik = post('is_mikrotik');
        $product->save();
        Flash::success('Produk berhasil di tambah');
        return Redirect::refresh();
    }

    public function onDelete()
    {
        ProductCategory::whereParameter(post('parameter'))->first()->delete();
        Flash::success('Produk berhasil di hapus');
        return Redirect::refresh();
    }
}
