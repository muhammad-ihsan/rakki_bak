<?php namespace Web\Admin;

use Backend;
use System\Classes\PluginBase;

/**
 * Admin Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Admin',
            'description' => 'No description provided yet...',
            'author'      => 'Web',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Web\Admin\Components\AdminProduct'             => 'AdminProduct',
            'Web\Admin\Components\AdminProductDetail'       => 'AdminProductDetail',

            'Web\Admin\Components\AdminProvider'            => 'AdminProvider',
            'Web\Admin\Components\AdminProviderDetail'      => 'AdminProviderDetail',

            'Web\Admin\Components\AdminProviderPrice'       => 'AdminProviderPrice',
            'Web\Admin\Components\AdminProviderPriceDetail' => 'AdminProviderPriceDetail',

            'Web\Admin\Components\AdminCommerce'            => 'AdminCommerce',
            'Web\Admin\Components\AdminCommerceDetail'      => 'AdminCommerceDetail',

            'Web\Admin\Components\AdminUserMikrotik'            => 'AdminUserMikrotik',

            'Web\Admin\Components\AdminUser'                => 'AdminUser',
            'Web\Admin\Components\AdminUserDetail'          => 'AdminUserDetail',

            'Web\Admin\Components\IsLogin'                  => 'IsLogin',
            'Web\Admin\Components\IsAdministrator'          => 'IsAdministrator',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'web.admin.some_permission' => [
                'tab' => 'Admin',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'admin' => [
                'label'       => 'Admin',
                'url'         => Backend::url('web/admin/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['web.admin.*'],
                'order'       => 500,
            ],
        ];
    }
}
